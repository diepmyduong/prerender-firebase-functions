const functions = require('firebase-functions');
const express = require('express');
const path = require('path');
const prerenderUtil = require('./libs/util');
const prerenderNode = require('prerender-node');
const admin = require("firebase-admin"); admin.initializeApp(functions.config().firebase);
const hash = require("object-hash");

let app = express();
app.use('/', express.static(path.join(__dirname, './dist')));
app.use(prerenderNode.set('beforeRender', (req, done) => {
    console.log('before render');
    const url = prerenderUtil.getOptions(req).url;
    const cacheHash = hash(url);
    admin.database().ref(`cached`).child(cacheHash).on('value', snap => {
        const html = snap.val();
        if(html) {
            done(undefined, { body: html, status: 200 });
            snap.ref.off();
        } else {
            admin.database().ref(`request`).push(url);
        }
    })
}).set('afterRender', (err, req, prerender_res) => {
}));
app.get('**', (req, res) => {
    res.sendFile(path.join(__dirname, './dist/index.html'));
});

// app.set('port', 5050).listen(app.get('port'), function () {
//     console.log(`started at`)
// })

exports.ssr = functions.https.onRequest(app);